using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace AsyncFtpLib
{
    delegate void AsyncFtpErrorDelegate(string s);
    delegate void AsyncFtpProgressDelegate(string filename, int bytesTransferred, int bytesToTransfer);
    delegate void AsyncFtpTransferCompletedDelegate();

    class AsyncFtpTransfer
    {
        public AsyncFtpTransfer()
        {
            m_isDownload = false;
            m_transferSize = -1;
        }

        public bool SetUpload(string from, string to)
        {
            m_isDownload = false;
            m_from = from;
            m_to = to;

            if (System.IO.File.Exists(m_from))
            {
                FileInfo fileInfo = new FileInfo(m_from);
                m_transferSize = (int)fileInfo.Length;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SetDownload(string from, string to, int expectedSize = -1)
        {
            m_isDownload = true;
            m_from = from;
            m_to = to;
            return true;
        }

        public bool IsDownload()
        {
            return m_isDownload;
        }

        public string GetSource()
        {
            return m_from;
        }

        public string GetDestination()
        {
            return m_to;
        }

        public int GetTransferSize()
        {
            return m_transferSize;
        }

        private string m_from;
        private string m_to;
        private bool m_isDownload;
        private int m_transferSize;
    }

    class AsyncFtp
    {
        public AsyncFtp(string server, string rootPath, string username, string password)
        {
            m_credentials = new NetworkCredential(username, password);
            m_server = server;
            m_rootPath = rootPath;
            m_webClient = new WebClient();
            m_webClient.Credentials = m_credentials;
            m_webClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
            m_webClient.UploadFileCompleted += new UploadFileCompletedEventHandler(UploadFileCompleted);

            m_bytesToTransfer = 0;
            m_bytesTransferred = 0;
            m_bytesTransferring = 0;

            m_queue = new List<AsyncFtpTransfer>();
        }

        public bool Delete(String file)
        {
            String uri = m_server + m_rootPath + file;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(uri);
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            request.Credentials = m_credentials;

            FtpWebResponse response = null;
            
            try 
            {
                response = (FtpWebResponse) request.GetResponse();
                FtpStatusCode code = response.StatusCode;
                response.Close();
                return true;
            }
            catch (WebException)
            {
                if (response != null && response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    response.Close();
                    return true;
                }
            }

            if (response != null)
            {
                m_errorDelegate(String.Format("Failed to delete '{0}': {1}", file, response.StatusDescription));
            }
            return false;
        }

        public void SetErrorDelegate(AsyncFtpErrorDelegate errorDelegate)
        {
            m_errorDelegate = errorDelegate;
        }

        public void SetProgressDelegate(AsyncFtpProgressDelegate progressDelegate)
        {
            m_progressDelegate = progressDelegate;
        }

        public void SetTransferCompletedDelegate(AsyncFtpTransferCompletedDelegate transferCompletedDelegate)
        {
            m_transferCompletedDelegate = transferCompletedDelegate;
        }

        private void ProcessQueue()
        {
            if (m_queue.Count == 0)
                return;

            AsyncFtpTransfer transfer = m_queue.First();
            if (transfer.IsDownload())
            {
                m_queue.RemoveAt(0);
            }
            else
            {
                m_webClient.UploadFileAsync(new Uri(transfer.GetDestination()), transfer.GetSource());
            }
        }

        public void QueueUpload(string source, string destination)
        {
            AsyncFtpTransfer transfer = new AsyncFtpTransfer();
            string fullDestination = m_server + destination;
            bool success = transfer.SetUpload(source, fullDestination);
          
            if (success)
            {
                if (transfer.GetTransferSize() != -1)
                {
                    m_bytesToTransfer += transfer.GetTransferSize();
                }

                m_queue.Add(transfer);
                if (m_queue.Count() == 1)
                    ProcessQueue();
            }
            else
            {
                m_errorDelegate(String.Format("Failed to add '{0}' to queue", source));
            }            
        }

        private void UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            m_bytesTransferring = (int)e.BytesSent;
            m_progressDelegate(m_queue.First().GetDestination(), m_bytesTransferred + m_bytesTransferring, m_bytesToTransfer);
        }

        private void UploadFileCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            Exception error = e.Error;

            if (error != null)
            {
                // We'll get error 553 if the file we want to upload would be in a folder that doesn't exist
                if (error.Message.Contains("(553)"))
                {
                    string directory = m_queue.First().GetDestination();
                    int trim = m_server.Length + m_rootPath.Length;
                    directory = directory.Substring(trim, directory.LastIndexOf('/') - trim + 1);

                    // Create each subfolder in turn...
                    int startIndex = 0;
                    int nextIndex = 0;
                    while ((nextIndex = directory.IndexOf('/', startIndex)) != -1)
                    {
                        WebRequest request = WebRequest.Create(m_server + m_rootPath + directory.Substring(0, nextIndex));
                        request.Method = WebRequestMethods.Ftp.MakeDirectory;
                        request.Credentials = m_credentials;
                        try
                        {
                            using (var resp = (FtpWebResponse)request.GetResponse()) { }
                        }
                        catch (Exception ex)
                        {
                            m_errorDelegate(ex.Message);
                        }
                        startIndex = nextIndex + 1;
                    }

                    // Process the queue again, which will try to upload this file again, this time with no directory errors
                    ProcessQueue();
                }
                else // Errors we don't have a way of fixing
                {
                    m_errorDelegate(error.Message + ": ");
                    if (error.InnerException != null)
                    {
                        m_errorDelegate("- " + error.InnerException.Message);
                    }
                }
            }
            else
            {
                m_bytesTransferred += m_bytesTransferring;
                m_bytesTransferring = 0;

                m_queue.RemoveAt(0); // Pop the queue
                if (m_queue.Count == 0)
                {
                    m_transferCompletedDelegate();
                }
                else
                {
                    ProcessQueue();
                }
            }
        }

        WebClient m_webClient;
        NetworkCredential m_credentials;
        string m_server;
        string m_rootPath;
        List<AsyncFtpTransfer> m_queue;

        // Delegates
        AsyncFtpErrorDelegate m_errorDelegate;
        AsyncFtpProgressDelegate m_progressDelegate;
        AsyncFtpTransferCompletedDelegate m_transferCompletedDelegate;

        int m_bytesTransferring;
        int m_bytesTransferred;
        int m_bytesToTransfer;
    }
}