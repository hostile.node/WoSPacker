﻿namespace WoSPacker
{
    partial class WoSPacker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxRootFolder = new System.Windows.Forms.TextBox();
            this.labelLocalRootFolder = new System.Windows.Forms.Label();
            this.groupBoxManifest = new System.Windows.Forms.GroupBox();
            this.ButtonCreateManifest = new System.Windows.Forms.Button();
            this.textBoxVersion = new System.Windows.Forms.TextBox();
            this.labelVersion = new System.Windows.Forms.Label();
            this.groupBoxServerInfo = new System.Windows.Forms.GroupBox();
            this.textBoxServerRootFolder = new System.Windows.Forms.TextBox();
            this.buttonSync = new System.Windows.Forms.Button();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.textBoxServer = new System.Windows.Forms.TextBox();
            this.labelServerRootFolder = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelUsername = new System.Windows.Forms.Label();
            this.labelServer = new System.Windows.Forms.Label();
            this.groupBoxLog = new System.Windows.Forms.GroupBox();
            this.labelFilename = new System.Windows.Forms.Label();
            this.labelProgress = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBoxManifest.SuspendLayout();
            this.groupBoxServerInfo.SuspendLayout();
            this.groupBoxLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxRootFolder
            // 
            this.textBoxRootFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRootFolder.Location = new System.Drawing.Point(112, 23);
            this.textBoxRootFolder.Name = "textBoxRootFolder";
            this.textBoxRootFolder.Size = new System.Drawing.Size(361, 20);
            this.textBoxRootFolder.TabIndex = 0;
            // 
            // labelLocalRootFolder
            // 
            this.labelLocalRootFolder.AutoSize = true;
            this.labelLocalRootFolder.Location = new System.Drawing.Point(6, 26);
            this.labelLocalRootFolder.Name = "labelLocalRootFolder";
            this.labelLocalRootFolder.Size = new System.Drawing.Size(59, 13);
            this.labelLocalRootFolder.TabIndex = 1;
            this.labelLocalRootFolder.Text = "Root folder";
            // 
            // groupBoxManifest
            // 
            this.groupBoxManifest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxManifest.Controls.Add(this.ButtonCreateManifest);
            this.groupBoxManifest.Controls.Add(this.textBoxRootFolder);
            this.groupBoxManifest.Controls.Add(this.labelLocalRootFolder);
            this.groupBoxManifest.Location = new System.Drawing.Point(12, 12);
            this.groupBoxManifest.Name = "groupBoxManifest";
            this.groupBoxManifest.Size = new System.Drawing.Size(479, 78);
            this.groupBoxManifest.TabIndex = 2;
            this.groupBoxManifest.TabStop = false;
            this.groupBoxManifest.Text = "Manifest";
            // 
            // ButtonCreateManifest
            // 
            this.ButtonCreateManifest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonCreateManifest.Location = new System.Drawing.Point(398, 49);
            this.ButtonCreateManifest.Name = "ButtonCreateManifest";
            this.ButtonCreateManifest.Size = new System.Drawing.Size(75, 23);
            this.ButtonCreateManifest.TabIndex = 5;
            this.ButtonCreateManifest.Text = "Create";
            this.ButtonCreateManifest.UseVisualStyleBackColor = true;
            this.ButtonCreateManifest.Click += new System.EventHandler(this.ButtonCreateManifest_Click);
            // 
            // textBoxVersion
            // 
            this.textBoxVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVersion.Location = new System.Drawing.Point(112, 119);
            this.textBoxVersion.Name = "textBoxVersion";
            this.textBoxVersion.Size = new System.Drawing.Size(361, 20);
            this.textBoxVersion.TabIndex = 7;
            this.textBoxVersion.Text = "0.0.1";
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(6, 122);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(42, 13);
            this.labelVersion.TabIndex = 8;
            this.labelVersion.Text = "Version";
            // 
            // groupBoxServerInfo
            // 
            this.groupBoxServerInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxServerInfo.Controls.Add(this.textBoxVersion);
            this.groupBoxServerInfo.Controls.Add(this.textBoxServerRootFolder);
            this.groupBoxServerInfo.Controls.Add(this.labelVersion);
            this.groupBoxServerInfo.Controls.Add(this.buttonSync);
            this.groupBoxServerInfo.Controls.Add(this.textBoxPassword);
            this.groupBoxServerInfo.Controls.Add(this.textBoxUsername);
            this.groupBoxServerInfo.Controls.Add(this.textBoxServer);
            this.groupBoxServerInfo.Controls.Add(this.labelServerRootFolder);
            this.groupBoxServerInfo.Controls.Add(this.labelPassword);
            this.groupBoxServerInfo.Controls.Add(this.labelUsername);
            this.groupBoxServerInfo.Controls.Add(this.labelServer);
            this.groupBoxServerInfo.Location = new System.Drawing.Point(12, 96);
            this.groupBoxServerInfo.Name = "groupBoxServerInfo";
            this.groupBoxServerInfo.Size = new System.Drawing.Size(479, 176);
            this.groupBoxServerInfo.TabIndex = 3;
            this.groupBoxServerInfo.TabStop = false;
            this.groupBoxServerInfo.Text = "Server info";
            // 
            // textBoxServerRootFolder
            // 
            this.textBoxServerRootFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxServerRootFolder.Location = new System.Drawing.Point(112, 94);
            this.textBoxServerRootFolder.Name = "textBoxServerRootFolder";
            this.textBoxServerRootFolder.Size = new System.Drawing.Size(361, 20);
            this.textBoxServerRootFolder.TabIndex = 9;
            // 
            // buttonSync
            // 
            this.buttonSync.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSync.Enabled = false;
            this.buttonSync.Location = new System.Drawing.Point(398, 147);
            this.buttonSync.Name = "buttonSync";
            this.buttonSync.Size = new System.Drawing.Size(75, 23);
            this.buttonSync.TabIndex = 1;
            this.buttonSync.Text = "Sync";
            this.buttonSync.UseVisualStyleBackColor = true;
            this.buttonSync.Click += new System.EventHandler(this.buttonSync_Click);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPassword.Location = new System.Drawing.Point(112, 70);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(361, 20);
            this.textBoxPassword.TabIndex = 8;
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxUsername.Location = new System.Drawing.Point(112, 46);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(361, 20);
            this.textBoxUsername.TabIndex = 7;
            // 
            // textBoxServer
            // 
            this.textBoxServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxServer.Location = new System.Drawing.Point(112, 23);
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.Size = new System.Drawing.Size(361, 20);
            this.textBoxServer.TabIndex = 6;
            // 
            // labelServerRootFolder
            // 
            this.labelServerRootFolder.AutoSize = true;
            this.labelServerRootFolder.Location = new System.Drawing.Point(6, 97);
            this.labelServerRootFolder.Name = "labelServerRootFolder";
            this.labelServerRootFolder.Size = new System.Drawing.Size(59, 13);
            this.labelServerRootFolder.TabIndex = 3;
            this.labelServerRootFolder.Text = "Root folder";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(6, 73);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(53, 13);
            this.labelPassword.TabIndex = 2;
            this.labelPassword.Text = "Password";
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Location = new System.Drawing.Point(6, 49);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(55, 13);
            this.labelUsername.TabIndex = 1;
            this.labelUsername.Text = "Username";
            // 
            // labelServer
            // 
            this.labelServer.AutoSize = true;
            this.labelServer.Location = new System.Drawing.Point(6, 26);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(38, 13);
            this.labelServer.TabIndex = 0;
            this.labelServer.Text = "Server";
            // 
            // groupBoxLog
            // 
            this.groupBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxLog.Controls.Add(this.labelFilename);
            this.groupBoxLog.Controls.Add(this.labelProgress);
            this.groupBoxLog.Controls.Add(this.progressBar);
            this.groupBoxLog.Controls.Add(this.textBoxLog);
            this.groupBoxLog.Location = new System.Drawing.Point(12, 272);
            this.groupBoxLog.Name = "groupBoxLog";
            this.groupBoxLog.Size = new System.Drawing.Size(479, 206);
            this.groupBoxLog.TabIndex = 4;
            this.groupBoxLog.TabStop = false;
            // 
            // labelFilename
            // 
            this.labelFilename.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFilename.Location = new System.Drawing.Point(9, 188);
            this.labelFilename.Name = "labelFilename";
            this.labelFilename.Size = new System.Drawing.Size(363, 13);
            this.labelFilename.TabIndex = 6;
            this.labelFilename.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProgress.Location = new System.Drawing.Point(367, 188);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(106, 13);
            this.labelProgress.TabIndex = 5;
            this.labelProgress.Text = "--/--";
            this.labelProgress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(6, 163);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(467, 23);
            this.progressBar.TabIndex = 2;
            // 
            // textBoxLog
            // 
            this.textBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLog.Location = new System.Drawing.Point(6, 14);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLog.Size = new System.Drawing.Size(467, 143);
            this.textBoxLog.TabIndex = 0;
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 5000;
            this.toolTip.InitialDelay = 100;
            this.toolTip.ReshowDelay = 500;
            this.toolTip.UseAnimation = false;
            this.toolTip.UseFading = false;
            // 
            // WoSPacker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 485);
            this.Controls.Add(this.groupBoxLog);
            this.Controls.Add(this.groupBoxServerInfo);
            this.Controls.Add(this.groupBoxManifest);
            this.Name = "WoSPacker";
            this.Text = "Wings of Steel Packer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Packer_FormClosing);
            this.Load += new System.EventHandler(this.Packer_Load);
            this.groupBoxManifest.ResumeLayout(false);
            this.groupBoxManifest.PerformLayout();
            this.groupBoxServerInfo.ResumeLayout(false);
            this.groupBoxServerInfo.PerformLayout();
            this.groupBoxLog.ResumeLayout(false);
            this.groupBoxLog.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxRootFolder;
        private System.Windows.Forms.Label labelLocalRootFolder;
        private System.Windows.Forms.GroupBox groupBoxManifest;
        private System.Windows.Forms.Button ButtonCreateManifest;
        private System.Windows.Forms.GroupBox groupBoxServerInfo;
        private System.Windows.Forms.Label labelServerRootFolder;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.Label labelServer;
        private System.Windows.Forms.GroupBox groupBoxLog;
        private System.Windows.Forms.Button buttonSync;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.TextBox textBoxServerRootFolder;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.TextBox textBoxServer;
        private System.Windows.Forms.Label labelFilename;
        private System.Windows.Forms.TextBox textBoxVersion;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.ToolTip toolTip;
    }
}

