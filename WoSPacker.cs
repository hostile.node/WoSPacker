﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Threading.Tasks;
using AsyncFtpLib;
using WoSManifestLib;

namespace WoSPacker
{
    public partial class WoSPacker : Form
    {
        public WoSPacker()
        {
            InitializeComponent();
        }

        private String GetTemporaryFolder()         { return "temp"; }
        private String GetPreferencesFilename()     { return "preferences.xml"; }
        private String GetManifestFilename()        { return "manifest.xml"; }

        private void Packer_FormClosing(object sender, FormClosingEventArgs e)
        {
            SavePreferences();
        }

        private void Packer_Load(object sender, EventArgs e)
        {
            LoadPreferences();

            toolTip.SetToolTip(textBoxRootFolder, "Root folder containing all files / folders which we want to pack and upload.");
            toolTip.SetToolTip(textBoxServer, "FTP server address.");
            toolTip.SetToolTip(textBoxUsername, "Server's username. Can be empty if the server allows anonymous login.");
            toolTip.SetToolTip(textBoxPassword, "Server's password. Can be empty if the server allows anonymous login.");
            toolTip.SetToolTip(textBoxServerRootFolder, "Root folder we'll upload to. Can be empty.");
            toolTip.SetToolTip(textBoxVersion, "Pack version number.");

            // Create temporary folder to hold the manifest and compressed files
            if (System.IO.Directory.Exists(GetTemporaryFolder()) == false)
            {
                System.IO.Directory.CreateDirectory(GetTemporaryFolder());
            }
        }

        private void ButtonCreateManifest_Click(object sender, EventArgs e)
        {
            CreateManifest();
        }

        private void buttonSync_Click(object sender, EventArgs e)
        {
            textBoxLog.Text = "";
            SyncContents();
        }

        private void SavePreferences()
        {
            XmlWriter writer = XmlWriter.Create(GetTemporaryFolder() + "\\" + GetPreferencesFilename());
            writer.WriteStartDocument();
            writer.WriteStartElement("Preferences");
            writer.WriteElementString("RootFolder", textBoxRootFolder.Text);
            writer.WriteElementString("Server", textBoxServer.Text);
            writer.WriteElementString("Username", textBoxUsername.Text);
            writer.WriteElementString("ServerRootFolder", textBoxServerRootFolder.Text);
            writer.WriteElementString("Version", textBoxVersion.Text);
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
        }

        private void LoadPreferences()
        {
            if (System.IO.File.Exists(GetTemporaryFolder() + "\\" + GetPreferencesFilename()))
            {
                XmlReader reader = XmlReader.Create(GetTemporaryFolder() + "\\" + GetPreferencesFilename());
                while (reader.Read())
                {
                    if (reader.IsStartElement() && !reader.IsEmptyElement)
                    {
                        if (reader.Name == "RootFolder")
                        {
                            reader.Read();
                            textBoxRootFolder.Text = reader.Value;
                        }
                        else if (reader.Name == "Server")
                        {
                            reader.Read();
                            textBoxServer.Text = reader.Value;
                        }
                        else if (reader.Name == "Username")
                        {
                            reader.Read();
                            textBoxUsername.Text = reader.Value;
                        }
                        else if (reader.Name == "ServerRootFolder")
                        {
                            reader.Read();
                            textBoxServerRootFolder.Text = reader.Value;
                        }
                        else if (reader.Name == "Version")
                        {
                            reader.Read();
                            textBoxVersion.Text = reader.Value;
                        }
                    }
                }
                reader.Close();
            }
        }

        public void Log(string str)
        {
            textBoxLog.AppendText(str + Environment.NewLine);;
            textBoxLog.Refresh();
        }

        private void CreateManifest()
        {
            textBoxLog.Clear();
            Log("Creating manifest...");

            if (System.IO.Directory.Exists(textBoxRootFolder.Text) == false)
            {
                Log("ERROR: Root folder doesn't exist.");
                return;
            }

            localManifest = new Manifest();
			if (localManifest.GenerateFromDirectory(textBoxRootFolder.Text) == false)
			{
                Log("ERROR: Couldn't read directory to create manifest.");
                return;
            }

            CompressContents();
        }

        private async Task CompressContents()
        {
            int totalFilesToCompress = localManifest.Entries.Count;
            int totalFilesCompressed = 0;
            if (totalFilesToCompress == 0)
            {
                OnCompressionFinished();
                return;
            }

            Log("Compressing contents...");
            progressBar.Value = 0;

            String compressedFolder = GetTemporaryFolder() + "\\compressed";

            if (System.IO.Directory.Exists(compressedFolder) == false)
            {
                System.IO.Directory.CreateDirectory(compressedFolder);
            }

            long totalSize = 0;
            long totalCompressedSize = 0;
            foreach (ManifestEntry entry in localManifest.Entries)
            {
                String compressedFilename = entry.ShortFilename;
                compressedFilename = compressedFolder + "\\" + compressedFilename + ".wospacked";

                FileInfo fileToCompress = new FileInfo(entry.FullFilename);
                long fileToCompressSize = fileToCompress.Length;
                long fileToCompressSizeKb = (long)((float)fileToCompress.Length / 1024.0f);

                int lastSlashIndex = compressedFilename.LastIndexOf('\\');
                if (lastSlashIndex != -1)
                {
                    String path = compressedFilename.Substring(0, lastSlashIndex);
                    if (System.IO.Directory.Exists(path) == false)
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                }

                if (fileToCompressSize == 0)
                {
                    Log(String.Format("Skipping compression of {0}, empty file.", fileToCompress.Name));
                }
                else
                {
                    Log(String.Format("Compressing '{0}'... ", entry.ShortFilename));

                    await Task.Run(() =>
                    {
                        ProcessStartInfo p = new ProcessStartInfo();
                        p.FileName = "7za.exe";
                        p.Arguments = "a \"" + compressedFilename + "\" \"" + fileToCompress + "\" -mx=9";
                        p.WindowStyle = ProcessWindowStyle.Hidden;

                        Process x = Process.Start(p);
                        x.WaitForExit();
                    });

                    FileInfo compressedFileInfo = new FileInfo(compressedFilename);
                    long compressedSize = compressedFileInfo.Length;
                    long compressedSizeKb = (long)((float)compressedSize / 1024.0f);

                    entry.CompressedSize = compressedSize;
                    totalCompressedSize += compressedSize;
                    totalFilesCompressed++;

                    progressBar.Value = (int)((float)totalFilesCompressed / (float)totalFilesToCompress * 100.0f);
                    labelProgress.Text = String.Format("{0}/{1}", totalFilesCompressed, totalFilesToCompress);
                }

                totalSize += fileToCompressSize;
            }

            long totalSizeKb = (long)((float)totalSize / 1024.0f);
            long totalCompressedSizeKb = (long)((float)totalCompressedSize / 1024.0f);
            Log(String.Format("Compression finished, from {0}kb to {1}kb ({2:P1} compressed).",
                totalSizeKb,
                totalCompressedSizeKb,
                1.0f - (float)totalCompressedSize / (float)totalSize));

            OnCompressionFinished();
        }

        private void OnCompressionFinished()
        {
            // Write out the manifest file. This can only happen after the compression has finished as the
            // manifest's entries contain the compressed size of the file.
            localManifest.WriteToFile(GetTemporaryFolder() + "\\" + GetManifestFilename());

            buttonSync.Enabled = true;
            progressBar.Value = 0;
            labelProgress.Text = "Done";
            Log("Manifest created, ready to sync.");
        }

        private void OnFtpError(string str)
        {
            Log(str);
        }

        private void OnFtpProgress(string filename, int bytesTransferred, int bytesToTransfer)
        {
            int progress = 0;
            if (bytesToTransfer > 0)
            {
                progress = (int)(100.0f * (float)bytesTransferred / (float)bytesToTransfer);
            }

            labelProgress.Text = String.Format("{0}/{1}kb", (int)((float)bytesTransferred / 1024f), (int)((float)bytesToTransfer / 1024.0f));
            int clampedIndex = filename.Length - 50;
            if (clampedIndex < 0) clampedIndex = 0;
            labelFilename.Text = "..." + filename.Substring(clampedIndex);

            progressBar.Value = progress;
        }

        private bool SyncContents()
        {
            if (textBoxServer.Text.Length == 0)
            {
                Log("ERROR: Server address field can't be empty.");
                return false;
            }
            else if (textBoxVersion.Text.Length == 0)
            {
                Log("ERROR: Version field can't be empty.");
                return false;
            }

            // A new version file must be uploaded after we upload everything else.
            uploadVersionFile = true;

            String serverAddress = textBoxServer.Text;
            if (serverAddress.StartsWith("ftp://") == false)
            {
                serverAddress = "ftp://" + serverAddress;
            }

            ftp = new AsyncFtp(serverAddress, textBoxServerRootFolder.Text, textBoxUsername.Text, textBoxPassword.Text);
            ftp.SetErrorDelegate(new AsyncFtpErrorDelegate(OnFtpError));
            ftp.SetProgressDelegate(new AsyncFtpProgressDelegate(OnFtpProgress));
            ftp.SetTransferCompletedDelegate(new AsyncFtpTransferCompletedDelegate(OnFtpTransferCompleted));

            Log("Queuing files and transferring");

            SetServerRoot();

            ftp.QueueUpload(GetTemporaryFolder() + "\\" + GetManifestFilename(), serverRoot + textBoxVersion.Text + "/" + GetManifestFilename());

            // The transfers are asynchronous, so once they're done OnFtpTransferCompleted() will be called.
            String compressedFolder = textBoxRootFolder.Text + "\\compressed";
            foreach (ManifestEntry entry in localManifest.Entries)
            {
                String localFile = GetTemporaryFolder() + "\\compressed\\" + entry.ShortFilename + ".wospacked";
                String remoteFile = serverRoot + textBoxVersion.Text + "/" + entry.ShortFilename + ".wospacked";
                remoteFile = remoteFile.Replace('\\', '/');
                ftp.QueueUpload(localFile, remoteFile);
            }

            return true;
        }

        private void SetServerRoot()
        {
            // Make sure that our root folder forms a sensible path
            serverRoot = "/";
            if (textBoxServerRootFolder.Text.Length > 0)
            {
                serverRoot += textBoxServerRootFolder.Text;
                if (serverRoot.EndsWith("/") == false)
                {
                    serverRoot += "/";
                }
            }
        }

        private void OnFtpTransferCompleted()
        {
            if (uploadVersionFile == true)
            {
                Log("Files transferred, uploading new version file...");
                CreateVersionFile();
                UploadVersionFile();
            }
            else
            {
                Log("Completed.");
            }
        }

        private void CreateVersionFile()
        {
            StreamWriter versionFile = File.CreateText(GetTemporaryFolder() + "\\version.txt");
            versionFile.Write(textBoxVersion.Text);
            versionFile.Close();
        }

        private void UploadVersionFile()
        {
            ftp.QueueUpload(GetTemporaryFolder() + "\\version.txt", serverRoot + "version.txt");
            uploadVersionFile = false;
        }

        private Manifest localManifest;
        private Boolean uploadVersionFile;
        private String serverRoot;
        AsyncFtp ftp;
    }
}
