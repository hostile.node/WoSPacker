# Packer
Packer is used to compress a folder and generate a manifest file.

# Credits

Project icon by [Freepik](https://www.freepik.com) from [FlatIcon](https://www.flaticon.com/), licensed by [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/).
